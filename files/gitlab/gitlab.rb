# The URL through which GitLab will be accessed.
external_url "https://git.ipa.al.local/"

# gitlab.yml configuration
gitlab_rails['time_zone'] = "Europe/Paris"
gitlab_rails['backup_keep_time'] = 604800
gitlab_rails['gitlab_email_enabled'] = true

# Default Theme
gitlab_rails['gitlab_default_theme'] = "2"

# Whether to redirect http to https.
nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = "/etc/gitlab/ssl/gitlab.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/gitlab.key"

# The directory where Git repositories will be stored.
git_data_dirs({
	"default" => {"path" => "/var/opt/gitlab/git-data"},
	"ceph-gitlab" => {"path" => "/mnt/ceph-gitlab"}
})

# The directory where Gitlab backups will be stored (Ceph FS)
gitlab_rails['backup_path'] = "/mnt/ceph-gitlab-backup"

# These settings are documented in more detail at
# https://gitlab.com/gitlab-org/gitlab-ce/blob/master/config/gitlab.yml.example#L118
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = YAML.load_file('/etc/gitlab/freeipa_settings.yml')
gitlab_rails['prevent_ldap_sign_in'] = false

# GitLab Nginx
## See https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/nginx.md

# Use smtp instead of sendmail/postfix
# More details and example configuration at
# https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/smtp.md
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "smtp.gmail.com"
gitlab_rails['smtp_port'] = 587
gitlab_rails['smtp_user_name'] = "contact@antoinethys.com"
gitlab_rails['smtp_password'] = "un peu plus et j'oubliais celui là"
gitlab_rails['smtp_domain'] = "smtp.gmail.com"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = false
gitlab_rails['smtp_openssl_verify_mode'] = 'peer' # Can be: 'none', 'peer', 'client_once', 'fail_if_no_peer_cert', see http://api.rubyonrails.org/classes/ActionMailer/Base.html

# To change other settings, see:
# https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#changing-gitlab-yml-settings
