# Projet UF-INFRA version Vagrant/Ansible



Cette documentation suppose que vous avez déjà pris connaissance de la documentation du projet UF-INFRA à la racine de ce dépôt.

Documentation officielle des 2 principaux outils utilisés ici :

- [Vagrant](https://www.vagrantup.com/docs/)
- [Ansible](https://docs.ansible.com/ansible/latest/index.html)

## Présentation

Ce dossier contient un [Vagrantfile](./Vagrantfile) et un ensemble de Playbooks Ansible permettant de déployer facilement une version allégée du projet UF-INFRA.



La fichier `Vagrantfile` permet de créer 4 machines :

- `ipa.al.lan` : Serveur Freeipa utiliser pour la gestion des utilisateurs
- `git.al.lan` : Serveur git contenant le code de l'application web
- `app.al.lan` : Serveur web (`apache`) contenant l'application et une base de données (`mariadb`)
- `storage.al.lan` : Serveur de stockage utilisant `minio` pour fournir un stockage s3 à l'application

## Prérequis

Dépendences logiciels [ici](requirements.txt).

En plus de Vagrant je recommande son plugin `vagrant-hostsupdater` qui permet d'ajouter les machines Vagrants au fichier `/etc/hosts` automatiquement.

Pour l'exécution des playbook un fichier `ansible-vault` est nécessaire, doit se situer dans `provisioning/group_vars/` et se nommer `secret.yml`.\
`provisioning/group_vars/secret.yml`\
Son mot de passe sera demandé pendant l'étape de provisionning de Vagrant.
Documentation de `ansible-vault` [ici](https://docs.ansible.com/ansible/latest/user_guide/vault.html).

*Testé avec :*
- *`Vagrant 2.2.6` sur Manjaro Linux (Kernel Arch), utilisant le provider `vmware_desktop`, les plugins `vagrant-vmware-desktop` et `vagrant-hostsupdater`.*
- *`Ansible 2.9.4` sur Manjaro Linux (Kernel Arch) et utilisant `Python 3.8.1`.*

## Préparation

Le fichier `provisioning/group_vars/secret.yml` doit être un fichier `Ansible-Vault` et contenir les variables suivantes :

 - all_password (Mot de passe maitre utiliser pour l'administrateur)
 - deployer_pass (Mot de passe de l'utilisateur de déploiement)
 - db_pass (Mot de passe de l'utilisateur de la BDD)

Comme dans cette exemple :

```bash
$ ansible-vault create provisioning/group_vars/secret.yml
```

```yaml
all_password: "P@ssw0rd"
deployer_pass: "P@ssw0rd"
db_pass: "P@ssw0rd"
```

Comme ces fichiers ont été créés afin de réaliser un projet un utilisateur "client" au nom de `Antoine Thys` est créé avec pour mot de passe le contenu de la variable `all_password` et appartient au groupe `dev`, ce comportement se trouve dans le fichier `provisioning/roles/ipausers/tasks/users.yml` pour les utilisateurs et `provisioning/roles/ipausers/tasks/groups.yml` pour les groupes.\
Ce playbook permet également de définir les utilisateurs et groupes nécessaires, les utilisateurs du groupe `dev` pourront utiliser le dépôt git et tous les utilisateurs pourront se connecter à l'application web.\
La documentation relatif à la gestion des utilisateurs ipa via ansible est disponible [ici](https://docs.ansible.com/ansible/latest/modules/ipa_user_module.html).

Le fichier contenant les hôtes se trouve dans le dossier `provisioning`, `provisioning/hosts`. Si le `Vagrantfile` n'est pas modifier aucune modofication est nécessaire. 

## Déploiement

Pour déployer peut se réalise en une seule commande dans le dossier `deploy`:

```bash
$ vagrant up
```

Cette commande va créer les 4 machines définis dans le `Vagrantfile` puis utiliser `Ansible` pour faire la provision de ces machines.\
*Cette étape peut être assez longue, en particulier pendant l'installation des paquets.*

En cas d'erreur la commande :

```bash
$ vagrant provision {nom_de_la_machine_a_provisionner}
```

Permet de relancer le provisionnement.

Une fois la commande lancée le mot de passe pour ansible vault sera demandé pour le provisionnement de chaque machine.

## Utilisation

### Gestion des utilisateurs

Les utilisateurs sont définis dans le rôle `ipausers` comme documenté plus haut.\
Pour en ajouter il suffit de modifier le fichier et de relancer une provision sur la machine `ipa`.

```bash
$ vagrant provision ipa.al.lan
```

### Modification du code

Pour la modification du code il suffit de cloner le dépôt en local `utilisateur@git.al.lan:/srv/git/app.git`, d'effectuer les modification et de faire un `push` puis effectuer une une provision du serveur web.

Exemple :

```bash
$ antoinethys@git.al.lan:/srv/git/app.git
$ cd app
$ vim config/.env
$ git add config/.env
$ git commit -m "Edit .env file"
$ git push
$ vagrant provision app.al.lan
```
