# UF-infra

## Sommaire

- <a href="#install-ad">Installation d'un AD</a>
- <a href="#install-ceph">Installation de Ceph</a>
- <a href="#install-freeipa">Installation de FreeIPA</a>
- <a href="#install-trustadipa">Trust entre AD et IPA</a>
- <a href="#install-phpdeploy">Configuration de PHP Deploy</a>
- <a href="#install-monitoring">Monitoring avec Prometheus + Grafana</a>
  - <a href="#install-prometheus">Prometheus</a>
  - <a href="#install-grafana">Grafana</a>
- <a href="configure-gitlabci">GitLab CI/CD</a>

## <a name="install-ad"></a>Installation d'un Active Directory

### Pérequis 
- Un serveur avec Windows Serveur
- Une adresse IP Statique sur le serveur.
- Droits administrateurs sur la session Windows.

### Installation

- Dans le **Gestionnaire de serveur**, cliquez sur **Gérer**, puis sur **Ajout de rôles et de fonctionnalités** pour démarrer l’Assistant Ajout de rôles.

- Dans la page **Avant de commencer** , cliquez sur **Suivant**.

- Dans la page **Sélectionner le type d’installation**, cliquez sur **Installation basée sur un rôle ou une fonctionnalité**, puis sur **Suivant**.

- Dans la page **Sélectionner le serveur de destination**, cliquez sur **Sélectionner un serveur du pool de serveurs**, cliquez sur le nom du serveur sur lequel vous voulez installer les services Active Directory, puis cliquez sur **Suivant**.

- Dans la page **Sélectionner des rôles de serveurs**, cliquez sur **Services de domaine Active Directory**. Ensuite, dans la boîte de dialogue **Assistant Ajout de rôles et de fonctionnalités**, cliquez **Ajouter des fonctionnalités**, puis sur **Suivant**.

- Dans la page **Sélectionner des fonctionnalités**, cliquez sur **Suivant**.

- Dans **Services de domaine Active Directory**, cliquez sur **Suivant**.

- Dans la page **Confirmer les sélections d’installation**, cliquez sur **Installer**.

- Dans la page **Résultats**, vérifiez que l’installation s’est correctement déroulée, puis cliquez sur **Promouvoir ce serveur en contrôleur de domaine** pour démarrer l’Assistant Configuration des services de domaine Active Directory.

## <a name="install-ceph"></a>Installer un système de stockage Object avec Ceph :

## Ceph:

### Prérequis :
- Trois serveurs avec un utilisateur dédié a l'utilisation de Ceph.
- Ceph-deploy d'installé. [Installer ceph deploy](https://docs.ceph.com/docs/master/start/quick-start-preflight/#ceph-deploy-setup)
- Les droits root sur le serveur où ceph-deploy est installé.

### Installation

- En root : Sur les serveur prinpipal du cluster, créer un compte pour ceph, et générer une nouvelle pair de clés SSH:

```console
root@cephserver1:~# adduser cephuser
root@cephserver1:~# ssh-keygen
```

- Et il faudra copier la nouvelle clé SSH vers les autres serveurs du cluster. Ceci donne la possibilitée de `sudo` sans utilisation de mot de passe et sécurisare grandement les comminucations SSH.

> Ne pas oublier de copier la clé ssh sur le serveur 1, il faut ajouter la clé de l'utilsateur **root** à **cephuser**.

```console
root@cephserver1:~# ssh-copy-id cephuser@cephserver1
root@cephserver1:~# ssh-copy-id cephuser@cephserver2
root@cephserver1:~# ssh-copy-id cephuser@cephserver3
root@cephserver1:~# ...
```Centos 7 / 8

- Il manque juste à authoriser notre **cephuser** à utiliser la commande ceph-deploy en `sudo` sans mot de passe.

```console
root@ceph1:~# echo "cephuser ALL=(root) NOPASSWD:ALL" >/etc/sudoers.d/cephuser root@ceph1:~# service sudo reload
```

- Nous sommes maintenant prêt à utiliser la commande `ceph-deploy`, pour une question de propreté, on va créer un dossier dans l'home (~) ce root. l'exécution de la commande `ceph-deploy` va créer plusieurs autre fichiers.

```console
root@cephserver1:~# mkdir cephdeploy
root@cephserver1:~# cd cephdeploy
root@cephserver1:~# ceph-deploy --username cephuser new cephserver1 cephserver2 cephserver3 ...
```

On peut maintenant aller dans le fichier `ceph.conf` et vérifier la configuration. Ensuite on lance la commande :

> Même noms que dans la commande précédente, avec le mot clé `install`.

```console
root@cephserver1:~# ceph-deploy --username cephuser install cephserver1 cephserver2 cephserver3 ...
```

## S3 avec Ceph

A part de ce moment, Ceph est directement compatible avec la majorité des fonctionnalités du SDK S3. Il n'y a pas plus de manipulation à faire pour le rendre apte à utiliser et/ou comprendre S3.

## <a name="install-freeipa"></a>Installer FreeIPA

### Prérequis
- Installation Fedora ou RHEL.
- Une adresse IP statique.
- Un nom de domaine complet (ipa.exemple.fr).

> L'IP et le nom de domaine seront à titre d'exemple.
> IP Statique : 192.168.100.100
> Nom de domaine : ipa.exemple.fr

### Installation (RHEL)

- Premièrement faut définir la route dans le fichier `etc/hosts` avec le nom de domaine complet.

```bash
192.168.100.100    ipa.example.com
```

- On désactive SELINUX pour l'ouverture des ports de FreeIPA.

```bash
setenforce Permissive --permanent
```

- On peut maintenant directement passer à l'installation des packet de FreeIPA :

```bash
sudo yum install ipa-server ipa-server-dns -y
```

- Au cours de l'installation :

```bash
Configurer le DNS intégré : yes
Adresse du serveur : ipa.exemple.fr
Confirmer le nom de domaine : exemple.fr
Realm : EXEMPLE.FR
MDP Directory manager : <Entrer un mot de passe>
MDP Admin IPA : <Entrer un mot de passe>
Configurer la redirection de ports : yes
Configurer avec les valeurs par défaut : yes
Recherche pour les zones manquantes : no
Commencer l'installation avec cette configuration : yes
```

- Ajouter les règle de FreeIPA au pare-feu.

Les règles du à ajouter au pare feu sont les suivantes :

Ports TCP :

- 80, 443 : HTTP/HTTPS
- 389, 636 : LDAP/LDAPS
- 88, 464 : kerberos
- 53 : Bind (DNS)

Ports UDP :

- 88, 464 : kerberos
- 53 : Bind (DNS)
- 123 : ntp

Voici donc la commande pour toute les ajouter.

```bash
firewall-cmd --permanent --add-port={80/tcp, 88/tcp, 443/tcp, 389/tcp, 636/tcp, 88/tcp, 464/tcp, 53/tcp, 135/tcp, 138/tcp, 139/tcp, 445/tcp, 1024-1300/tcp, 88/udp, 464/udp, 53/udp, 123/udp, 138/udp, 139/udp, 389/udp, 445/udp}
firewall-cmd --reload
```

- On vérifie que toute l'installation de FreeIPA fonctionne correctement.

```bash
ipactl status
```

- Enfin peu accéder à l'interface en ligne de notre serveur FreeIPA suis L'URL :
> https://ipa.exemple.fr/ipa/ui

Et se connecter avec les identifiants de notre compte adminitrateur dont le nom d'utilisateur est `admin`.

## <a name="install-trustadipa"></a>Ajouter une relation d'approbation entre IPA et AD

### Prérequis

- Packet `ipa-server-trust-ad`

- Pour commencer on désactive totalement l'assigantion d'IPv6 pour éviter les attributions automatiques d'IPv6 aux cartes réseaux. Sur le serveur FreeIPA.

Dans le fichier `/etc/sysctl.d/ipv6.conf` :

```bash
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.<interface0>.disable_ipv6 = 1
```

- Sur le serveur AD (Windows Server) on ajoute nôtre serveur IPA en ligne de commende dans le PowerShell ou dans l'invite de commande.

En considérant que le nom de domaine complet de l'IPA est : ipa.exemple.fr
et que l'IP de l'IPA est : `192.168.100.100`.

```powershell
dnscmd 127.0.0.1 /ZoneAdd ipa.exemple.fr /Forwarder 192.168.100.100
```

- Puis sur le serveur IPA 

> NetBiosName sur ipa.exemple.fr = IPA
> 
> Sur ad.exemple.fr = AD
> 
> Sur quoiquecesoit.exemple.fr = QUOIQUECESOIT

```bash
ipa-adtrust-install --netbios-name=IPA -a mypassword1
```

- Pour finir on peu redemarrer tous les services de IPA :

```bash
systemctl restart krb5kdc sssd httpd
```

## <a name="install-phpdeploy"></a>Configuration du Deploiement de l'app web avec PHP Deploy

### Prérequis

- Un serveur Apache.
- SELinux mis en mode `Permissive`.
- Un dépo Git avec un Runner qui lance PHP Deploy.

### Configuration

- Premièrement il faut implanter `PHP Deploy` sur son App Web ([Doc PHP Deploy](https://deployer.org/docs/getting-started.html)).

- Ensuite il faut simplement configurer le fichier pour ocnvenir à vos besoins, et ajouter ces deux lignes.

```apacheconf
User deployer
Group deployer
```

- Et pour finir ajouter le nouvel utilisateur (ici `deployer`) dans le groupe de apache pour qu'il ai les droits de lecture et écriture sur `/var/www`.

## <a name="install-monitoring"></a>Installation d'un serveur de monitoring Prometheus avec dashboard web Grafana

### Prérequis

- Avoir un utilisateur pour le service Prometheus (avec le nom `prometheus`)
- La création des dossiers (vides) `/etc/prometheus` et `/var/lib/prometheus` & attribution de tous les droits pour l'utilisateur `prometheus` à ces dossiers.
- FACULTATIF : Installation de Node Exporter pour avoir des informations supplémentaires (utilisation du CPU, de la RAM, du(des) HDD/SSD). [DOC rapide](https://www.scaleway.com/en/docs/configure-prometheus-monitoring-with-grafana/#-Downloading-and-Installing-Node-Exporter)

### <a name="install-prometheus"></a>Installation

- On télécharge et extracte de Prometheus

```bash
wget https://github.com/prometheus/prometheus/releases/download/v2.16.0/prometheus-2.16.0.linux-amd64.tar.gz
tar xfz prometheus.tar.gz
cd prometheus
```

- Ensuite on copie les binaires vers `/usr/local/bin` et on y ajoute les permissions.

```bash
sudo cp ./prometheus /usr/local/bin/
sudo cp ./promtool /usr/local/bin/

sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo chown prometheus:prometheus /usr/local/bin/promtool
```

- Ensuite on copie les fichiers `console` et `console_libraries` vers `/etc/prometheus` et on y ajoute les permissions

```bash
sudo cp -r ./consoles /etc/prometheus
sudo cp -r ./console_libraries /etc/prometheus

sudo chown -R prometheus:prometheus /etc/prometheus/consoles
sudo chown -R prometheus:prometheus /etc/prometheus/console_libraries
```

- On peu maintenant supprimer le fichier téléchargé.

```bash
cd ..
rm -rf prometheus
rm prometheus-2.16.0.linux-amd64.tar.gz
```

### Configuration et lancement de Prometheus

- On créer le fichier de configuration pour Prometheus.

```bash
sudo nano /etc/prometheus/prometheus.yml
```

Avec cette configuration dedans (Copier-Coller): [Doc prometheus.yml](https://prometheus.io/docs/prometheus/latest/configuration/configuration/)

```yaml
global:
  scrape_interval:     15s
  evaluation_interval: 15s

rule_files:
  # - "first.rules"
  # - "second.rules"

scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']

# Avec Node Export d'installé :
# - job_name: 'node_exporter'
#   scrape_interval: 5s
#   static_configs:
#     - targets: ['localhost:9100']
```

- Ensuite on peu créer le service pour lancer automatiquement Prometheus au démarrage de l'hôte.

Dans un nouveau fichier `etc/systemd/system/prometheus.service`

```ini
[Unit]
  Description=Prometheus Monitoring
  Wants=network-online.target
  After=network-online.target

[Service]
  User=prometheus
  Group=prometheus
  Type=simple
  ExecStart=/usr/local/bin/prometheus \
  --config.file /etc/prometheus/prometheus.yml \
  --storage.tsdb.path /var/lib/prometheus/ \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries
  ExecReload=/bin/kill -HUP $MAINPID

[Install]
  WantedBy=multi-user.target
```

- Pour finir on redémarre systemd, et on lance Prometheus maintenant et en automatique.

```bash
sudo systemctl daemon-reload
sudo systemctl enable prometheus
sudo systemctl start prometheus
```

A partir de maintenant on a accès on l'interface web de Prometheus à l'adresse suivante : `vote.domaine.fr:9000`

### <a name="install-grafana"></a>Installation de Grafana (RHEL)

Néanmoins cette interface web peut être nettement améliorée en installant Grafana en deux étapes simples.

- On installe Grafana

```bash
sudo yum install grafana -y
```

- Et on lance grafana maintenant et en automatique

```bash
sudo systemctl daemon-reload
sudo systemctl enable grafana-server
sudo systemctl start grafana-server
```

Maintenant à la même adresse on dispose d'une interface web beaucoup plus peaufinée avec comme compte par défaut `admin`/`admin`.

## <a name="configure-gitlabci"></a>Configuration du CI/CD Gitlab

La configuration du CI/CD pour l'application web est plutot simple, il suffis juste de faire en sorte que les runners fonctionnent correctement.

### Configuration

- Premièrement il faut faire en sorte que les paires de clés SSH soient bien partagés.

```bash 
mkdir -p ~/.ssh && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
eval $(ssh-agent -s)
echo "$DEPLOY_PRIVATE_KEY" | ssh-add -
```

- Ensuite (Spécifiquement pour cette application) il faut faire l'installation de toutes les dépendances de composer ainsi que composer lui même.

```bash
curl -s https://getcomposer.org/installer | php
php composer.phar install
```

- Et après avoir spécifié les services (Mysql par exemple) et les variables (Nom de la base de donnée et mot de passe) on peut lancer le script `deploy.php` avec un niveau 2 de `verbose` (sur 3 niveaux différents). 

> En effet gitlab garde en mémoire toutes les lignes de beacuoup des `runners` les plus récent sur un projet, ce qui permet d'avoir comme une page de log pas trop chargé mais qui donne suffisamment de précision en cas d'erreur.

```yaml
deploy:app:
  stage: deploy
  script:
    - vendor/bin/dep deploy -vv
```